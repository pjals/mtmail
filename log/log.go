/*
mtmail - minetest email verification whitelist
Copyright (C) 2022-2022 Daniel [REDACTED] <pjals.envs.net>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package log

import (
	"io"
	"os"
	"fmt"

	"github.com/rs/zerolog"
)

var L zerolog.Logger
var F *os.File
var I bool = false

func Init(logPath string, pretty bool) {
	file, err := os.OpenFile(logPath, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		os.Stderr.Write([]byte("failed to initialize logger: " + fmt.Sprintf("%v", err) + "\n"))
		os.Exit(1)
	}
	F = file
	var consoleWriter io.Writer
	if pretty {
		consoleWriter = zerolog.ConsoleWriter{Out: os.Stdout}
	} else {
		consoleWriter = os.Stdout
	}
	multi := zerolog.MultiLevelWriter(consoleWriter, F)
	L = zerolog.New(multi).With().Timestamp().Logger()

	I = true
}

func Close() {
	F.Close()
}
