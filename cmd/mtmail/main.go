/*
mtmail - minetest email verification whitelist
Copyright (C) 2022-2022 Daniel [REDACTED] <pjals.envs.net>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package main

import (
	"os"
	"runtime/debug"

	"git.envs.net/pjals/mtmail/log"
	"git.envs.net/pjals/mtmail/server"
	
	"github.com/urfave/cli/v2"
)

var GIT_COMMIT = func() string {
	if info, ok := debug.ReadBuildInfo(); ok {
		for _, setting := range info.Settings {
			if setting.Key == "vcs.revision" {
				return setting.Value
			}
		}
	}
	return "AGPLVIOLATION"
}()

func main() {
	app := &cli.App{
		Name: "mtmail",
		Usage: "minetest email verification whitelist",
		Flags: []cli.Flag{
			&cli.BoolFlag{
				Name: "pretty",
				Value: true,
				Usage: "use pretty logging (for console, not file)",
				Aliases: []string{"p"},
			},
			&cli.StringFlag{
				Name: "logPath",
				Value: os.DevNull,
				Usage: "log output",
				Aliases: []string{"o"},
			},
			&cli.StringFlag{
				Name: "password",
				Usage: "password for SMTP",
				Aliases: []string{"P"},
				EnvVars: []string{"MTMAIL_PASSWORD"},
				Required: true,
			},
			&cli.StringFlag{
				Name: "username",
				Usage: "email to authenticate to for SMTP",
				Aliases: []string{"u"},
				EnvVars: []string{"MTMAIL_EMAIL"},
				Required: true,
			},
			&cli.StringFlag{
				Name: "sender",
				Usage: "email to send as for SMTP, default is `username` flag",
				EnvVars: []string{"MTMAIL_SENDER"},
				Aliases: []string{"S"},
			},
			&cli.StringFlag{
				Name: "smtp",
				Usage: "SMTP host",
				EnvVars: []string{"MTMAIL_SMTP_HOST"},
				Aliases: []string{"s"},
				Required: true,
			},
			&cli.StringFlag{
				Name: "host",
				Usage: "where to host at, allows formats like '1.2.3.4:8080' and ':8080'",
				Value: ":8080",
				Aliases: []string{"H"},
			},
			&cli.StringFlag{
				Name: "whitelist",
				Aliases: []string{"w"},
				Usage: "whitelist filepath",
				Required: true,
			},
		},
		Action: func(ctx *cli.Context) error {
			log.Init(ctx.String("logPath"), ctx.Bool("pretty"))
			log.L.Info().Msg("mtmail version " + GIT_COMMIT[:10] + " initialized")
			var sender string
			if ctx.IsSet("sender") {
				sender = ctx.String("sender")
			} else {
				sender = ctx.String("username")
			}
			server.SMTPHost = ctx.String("smtp")
			server.Username = ctx.String("username")
			server.Password = ctx.String("password")
			server.Sender = sender
			server.HTTPHost = ctx.String("host")
			server.WhitelistFile = ctx.String("whitelist")
			server.AGPLNotice = `<p><small>This webapp is licensed under the AGPLv3 or later, Git commit <a href="https://git.envs.net/pjals/mtmail/commit/` + GIT_COMMIT + `">` + GIT_COMMIT[:10] + `</a></p>`
			server.Serve()
			log.Close()
			return nil
		},
	}

	if err := app.Run(os.Args); err != nil {
		if (!log.I) {
			log.Init(os.DevNull, true)
		}
		log.L.Error().Err(err).Msg("")
		log.Close()
		os.Exit(1)
	}	
}
