# `mtmail`

Minetest email verification whitelist

## Usage

See `--help`.

```bash
# Build
cd cmd/mtmail/
go build -buildvcs=true # important! if set to false, might cause an agpl violation!
mv mtmail ../..

# Example:
./mtmail -P 'super secret' -o 'log.txt' -u 'agent@example.com' -S 'mtmail@example.com' -s 'mail.example.com:465' -H ':80'
```

## License

This README is licensed under the GNU ALL-permissive license:
```
Copyright 2022, Daniel [REDACTED] <pjals.envs.net>

Copying and distribution of this file, with or without modification, are permitted in any medium without royalty, provided the copyright notice and this notice are preserved. This file is offered as-is, without any warranty.
```

The program is licensed under the GNU AGPLv3-or-later, see `LICENSE` or `LICENSE.md`.
