/*
mtmail - minetest email verification whitelist
Copyright (C) 2022-2022 Daniel [REDACTED] <pjals.envs.net>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package server

import (
	"net/smtp"
	"net/http"

	"github.com/mr-tron/base58"
	"crypto/rand"
	
	"os"
	"strings"
	"regexp"
	"io"
	"time"

	"git.envs.net/pjals/mtmail/log"
)

var (
	SMTPHost string
	Username string
	Password string
	Sender string
	HTTPHost string
	WhitelistFile string
	AGPLNotice string
)

func SendEmail(to string, subject string, body string) {
	auth := smtp.PlainAuth("", Username, Password, strings.Split(SMTPHost, ":")[0])
	err := smtp.SendMail(SMTPHost, auth, Sender, []string{to}, []byte(
"User-Agent: s-nail v14.9.22\r\n" +
"MIME-Version: 1.0\r\n" +
"Content-Type: text/plain; charset=us-ascii\r\n" +
"Content-Transfer-Encoding: quoted-printable\r\n" +
"Subject: " + subject + "\r\n" +
"From: " + Sender + "\r\n" +
"To:" + to + "\r\n" +
"X-Envelope-From: <" + Sender + ">\r\n" +
"X-Envelope-To: <" + to + ">\r\n" +
"Date: " + time.Now().Format(time.RFC1123Z) + "\r\n\r\n" +
body))
	if err != nil {
		log.L.Error().Err(err).Msg("failed to send email")
		os.Exit(1)
	}
}

func AddToWhitelist(player string) {
	file, err := os.OpenFile(WhitelistFile, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.L.Error().Err(err).Msg("failed to open whitelist file")
		os.Exit(1)
	}
	if _, err := file.Write([]byte(player + "\n")); err != nil {
		log.L.Error().Err(err).Msg("failed to write to whitelist file")
		os.Exit(1)
	}
	if err := file.Close(); err != nil {
		log.L.Error().Err(err).Msg("failed to close whitelist file")
		os.Exit(1)
	}
	log.L.Info().Msg("Player " + player + " whitelisted!")
}

func getToken(length int) string {
	randomBytes := make([]byte, length)
	_, err := rand.Read(randomBytes)
	if err != nil {
		panic(err)
	}
	return base58.Encode(randomBytes)[:length]
}

func Serve() {
	http.HandleFunc("/whitelist", func(w http.ResponseWriter, req *http.Request) {
		io.WriteString(w,
`
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>~vern Minetest Whitelist</title>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
	</head>
	<body>
		<form method="post" action="/form">
			<label>Username:
				<input name="username" />
			</label>
			<label>EMail:
				<input name="email" type="email" autocomplete="email" />
			</label>
			<button>Submit</button>
		</form>
		` + AGPLNotice + `
	</body>
</html>
`)
	})
	codes := make(map[string]string)
	http.HandleFunc("/form", func(w http.ResponseWriter, req *http.Request) {
		email := req.FormValue("email")
		username := req.FormValue("username")
		matched, err := regexp.MatchString(`^[^@ \t\r\n]+@[^@ \t\r\n]+\.[^@ \t\r\n]+$`, email)
		if err != nil {
			log.L.Error().Err(err).Msg("failed to use email regex")
			os.Exit(1)
		}
		var result string
		if (len(username) <= 15) && matched {
			code := getToken(32)
			codes[code] = username
			SendEmail(email, "~vern Minetest Verification", "The code is: " + code)
			result =
`<p>Please check ` + email + ` for new emails, we have sent a verification email there.</p>
<form method="post" action="/code">
	<label>Code:
		<input name="code" />
	</label>
	<button>Submit</button>
</form>`
		} else {
			result = `<p>Invalid email or username.</p>`
		}
		io.WriteString(w,
`<!DOCTYPE html>
<html lang="en">
	<head>
		<title>~vern Minetest Whitelist - Email sent</title>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
	</head>
	<body>` + result + " " + AGPLNotice + `</body>
</html>`)
	})

	http.HandleFunc("/code", func(w http.ResponseWriter, req *http.Request) {
		code := req.FormValue("code")
		var result string
		if username, ok := codes[code]; ok {
			delete(codes, code)
			AddToWhitelist(username)
			result = `<p>Welcome to ~vern Minetest! You have been whitelisted.</p>`
		} else {
			delete(codes, code)
			result = `<p>Something went wrong, try this process again.</p>`
		}
		io.WriteString(w,
`
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>~vern Minetest Whitelist - Email sent</title>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
	</head>
	<body>` + result + " " + AGPLNotice + `</body>
</html>
`)
	})

	http.HandleFunc("/", func(writer http.ResponseWriter, req *http.Request) {
		http.Redirect(writer, req, "/whitelist", http.StatusMovedPermanently)
	})
	log.L.Error().Err(http.ListenAndServe(HTTPHost, nil)).Msg("")
	os.Exit(1)
}
